var S = require('string');

module.exports = function exercise(foo) {
    foo.string = "It's easy for you.";
    foo.foundEasy = false;

    for (var i = 0; i < foo.string.length; i++) {
        if (S(foo.string.slice(i, foo.string.length-i)).startsWith('easy')) {
            foo.foundEasy = true;
        }
    }

    return foo;
};
