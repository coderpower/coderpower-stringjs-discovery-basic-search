var S = require('string');

module.exports = function basics(foo) {
    foo.string = "Hello coder power";
    
    foo.contains = S(foo.string).contains('coder');
    console.log('Contains', foo.contains);

    foo.startsWith = S(foo.string).startsWith('Hello');
    console.log('Starts with', foo.startsWith);

    foo.endsWith = S(foo.string).endsWith('power');
    console.log('Ends with', foo.endsWith);

    foo.left = S(foo.string).left(5).s;
    console.log('Left', foo.left);

    foo.right = S(foo.string).right(5).s;
    console.log('Right', foo.right);

    return foo;
};
