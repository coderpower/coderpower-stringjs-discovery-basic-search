var S = require('string');
var chai = require('chai');
var expect = chai.expect;

// Sources
var basics = require('../sources/basics');
var foo = basics({
    string: 'Hello coder power'
});

describe('', function() {
    it('should return an object', function(done) {
        expect(foo).to.be.an('object');
        done();
    });
    it('the object should have the keys: contains, startsWith, endsWith, left, right', function(done) {
        expect(foo.contains).to.exist;
        expect(foo.startsWith).to.exist;
        expect(foo.endsWith).to.exist;
        expect(foo.left).to.exist;
        expect(foo.right).to.exist;
        done();
    });
    it('The keys: contains, startsWith, endsWith should be true', function(done) {
        expect(foo.contains).to.be.true;
        expect(foo.startsWith).to.be.true;
        expect(foo.endsWith).to.be.true;
        done()
    });
    it('The key left should return "Hello" and the key right "power"', function(done) {
        expect(foo.left).to.eql('Hello');
        expect(foo.right).to.eql('power');
        done();
    });
});
