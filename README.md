### Basic search with String.js

In this example we will cover these functions:

```javascript
    S('Hello world').contains('world')
    S('Hello world').startsWith('Hello')
    S('Hello world').endsWith('world')
    S('Hello world').left(5).s
    S('Hello world').right(5).s
```

For more information, please refer to the documentation: http://stringjs.com.
